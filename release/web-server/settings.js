module.exports = {
	/* MongoDB Config */
	"dbUrl": "mongodb://10.19.203.106:27017",
	"dbName": "Bitcoin",
	"accountCollectionName": "Account",
	"productCollectionName": "Product",
	"forecastCollectionName": "Forecast",
	"orderBookCollectionName": "OrderBook",
	"newsCollectionName": "News"
};
