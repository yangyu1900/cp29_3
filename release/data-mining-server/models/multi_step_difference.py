import pandas as pd
import numpy as np
from numpy import fliplr
from numpy import flipud
from numpy import array
from pandas.io.json import json_normalize
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import json
import math
import pymongo
from pymongo import MongoClient
import time
from sklearn import metrics
from sklearn.metrics import confusion_matrix

def connect_database():
 
    client = MongoClient('localhost', 27017)
    db = client['bitcoin']
    collection = db['final_data2']
    return db, collection

def data_extraction(field, collection):
    '''
    extract a certain filed('close') of data from the collecttion 
    '''

    data = []
    ''' for tweet in list(tw.find())[-10300:-300]:
        sen.append(tweet['mean'])
        t.append(tweet['_id'])'''
    for doc in list(collection.find())[-10300]:
        data.append(doc[field])
    data = array(data)
    #print(len(data))
    first = data[0]
    #data = flipud(data)
    difference = np.zeros(len(data)-1)
    for i in range(1, len(data)):
        difference[i-1] = data[i] - data[i-1]
    return first, difference

def price_extraction(filed, collection):
    data = []
    for doc in list(collection.find()):
        data.append(doc[filed])
    data = array(data)
    #data = flipud(data)
    first = data[0]
    difference = np.zeros(len(data)-1)
    for i in range(1, len(data)):
        difference[i-1] = data[i] - data[i-1]
    return first, difference

def feature_engineering(df):
    """
    preprocess data for better result
    :return:
    """
def take_by_window(dataset, window):
    x, y = [], []
    for i in range(len(dataset) - window ):
        x.append(dataset[i:(i + window)])
        y.append(dataset[i + window,])
    x = np.array(x)
    print(x.shape)
    y = np.array(y)
    x = np.reshape(x, (x.shape[0],1 , x.shape[1]))
    print(x.shape)
    return x, y

def pred_model(x_train, y_train, step_size=1, num_features=50, epochs=100, batch_size=128, verbose=2, pred_num=300):
    """
    ML model: LSTM
    Look_back: how many point
    :return:
    """

    model = Sequential()
    model.add(LSTM(4, input_shape=(step_size, num_features), return_sequences=True))
    model.add(Dropout(0.2))  # drop out regulation
    model.add(LSTM(4))
    model.add(Dropout(0))
    model.add(Dense(1))  # output file size
    model.add(Activation('tanh'))  # active function
    
    model.compile(loss='mse', optimizer='adam')
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, verbose=verbose)
    
    
    
    pred_feature = x_train[-1]

    pred_feature = np.reshape(pred_feature, (1 ,1 , num_features))

    x_test = list([x_train[-1]])
    #pred_num = 300
    y_test = []
    x_test = []
    
    for i in range(pred_num):
        pred_data = model.predict(pred_feature)
        y_test.append(pred_data)
        pred_feature = np.append(pred_feature[0,0,1:], pred_data)
        #print(pred_feature)
        pred_feature = np.reshape(pred_feature, (1 ,1 , num_features))
        x_test.append(pred_feature)
    pred_train = model.predict(x_train)
    #pred_test = model.predict(x_test)
    #pred_dataset = array(pred_dataset)
    #print(array(x_test).shape, array(y_test).shape)
    
    #score = model.evaluate(array(x_test[0:-1]), y_test, batch_size=128)

    return pred_train , np.reshape(array(y_test), (pred_num))


def accuracy_cal(y_test, pred_test):
    assert len(y_test) == len(pred_test) 
    pred = []
    true = []
    for i in range(0, len(pred_test), 10):
        if pred_test[i+5] >= pred_test[i]:
            pred.append(1)
        else:
            pred.append(0)
        if pred_test[i+9] >= pred_test[i+5]:
            pred.append(1)
        else:
            pred.append(0)
            
        if y_test[i+5] >= y_test[i]:
            true.append(1)
        else:
            true.append(0)
        if y_test[i+9] >= y_test[i+5]:
            true.append(1)
        else:
            true.append(0)
    
    return true, pred

if __name__ == "__main__":
    np.random.seed(1234)
    db, collection = connect_database()
    first, dataset = price_extraction('close',collection)


    dataset = dataset.astype('float32')
    #time = time.astype('int32')
    mms = MinMaxScaler(feature_range=(-1, 1))

    '''construct a scale based on dataset'''
    dataset = mms.fit_transform(dataset)

    '''define the size of lookback'''
    window = 300
    '''divide dataset to training set and testing set'''
    train_size = int(len(dataset)-400)
    train_set, test_set = dataset[0:train_size], dataset[train_size:]
    '''prepare the training data'''
    x_train, y_train = take_by_window(train_set, window)
    y_test = test_set

    '''get the predict data'''
    start = time.time()
    pred_train, pred_test = pred_model(x_train, y_train, step_size=1, num_features = window, epochs=130, batch_size=128, verbose=2, pred_num = len(test_set))
    run_time = time.time()-start

    '''inverse the scale'''
    pred_train = mms.inverse_transform(pred_train)
    y_train = mms.inverse_transform(y_train)
    pred_test = mms.inverse_transform(pred_test)
    y_test = mms.inverse_transform(y_test)
    dataset = mms.inverse_transform(dataset)

    plt.figure(figsize=(10,8))
    plt.plot(range(100),pred_test[:100],label="predict")
    plt.plot(range(100),dataset[-400:-300],label = 'actual')
    plt.xlabel('Price (minute)')
    plt.ylabel('Price Difference (USD)')
    plt.legend()
    plt.show()

    real = first
    real_l = []
    real_l.append(first)
    for i in range(len(dataset)):
        real = real+dataset[i]
        real_l.append(real)
    test_l= []
    test = real_l[-400]
    for i in range(len(pred_test)):
        test= test+pred_test[i]
        test_l.append(test)

    plt.figure(figsize=(10,8))
    plt.plot(range(100),test_l[-400:-300],label="predict")
    plt.plot(range(100),real_l[-400:-300],label = 'actual')
    plt.xlabel('Time (minute)')
    plt.ylabel('Price (USD)')
    plt.legend()
    plt.show()
    
    plt.figure(figsize=(10,8))
    plt.plot(range(400),test_l,label="predict")
    plt.plot(range(400),real_l[-400:],label = 'actual')
    plt.xlabel('Time (minute)')
    plt.ylabel('Price (USD)')
    plt.legend()
    plt.show()

    v = list(map(lambda x: abs(x[0]-x[1]), zip(real_l[-400:], test_l))) 
    plt.figure(figsize=(10,8))
    plt.plot(range(400),v[-400:],label = 'error')
    plt.xlabel('Time (minute)')
    plt.ylabel('Error(USD)')
    plt.legend()
    plt.show()



    real, pred = accuracy_cal(list(real_l[0:100]), list(test_l[0:100]))    
    con_m = confusion_matrix(real, pred)
    print(con_m)
    print("Prediction score:\n%s" % metrics.classification_report(real, pred))
    
    trainScore = math.sqrt(mean_squared_error(y_train, pred_train[:, 0]))
    print('Train Score: %.2f MSE' % (trainScore))
    testScore = math.sqrt(mean_squared_error(y_test, pred_test[:]))
    print('Test Score: %.2f MSE' % (testScore))
    print('Run Time: ', run_time)
    
    
    
