#!/usr/bin/python

import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import os
import time
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation


def dataset_setup(dataset1=None, dataset2=None, look_back=None, sentiment=True, n=None):
    """
    """
    data_X = []
    data_X_2 = []
    data_y = []

    for i in range(len(dataset1) - look_back - 1 - n):
        price = dataset1[i:i + look_back, 0].tolist()
        if sentiment:
            sent = dataset2[i:i + look_back, 0].tolist()
            data_X_2.append(sent)
        data_X.append(price)
        data_y.append(dataset1[i + look_back + n, 0])

    return np.array(data_X), np.array(data_X_2), np.array(data_y)


def dataset_reshape(data_X):
    return np.reshape(data_X, (data_X.shape[0], data_X.shape[1], 1))


def dataset_gen(dataset1=None, dataset2=None, look_back=10, train_test_split=0.85, start_data=None,
                sentiment=False, n=None):
    """
    Use for data-extraction (JSON to pandas df?)
    input data from sentiment analysis
    Including the train/test data splitting
    :return:
    """

    train_size = int(len(dataset1) * train_test_split)
    train_set, test_set = dataset1[0:train_size, :], dataset1[train_size:, :]
    train_set2, test_set2 = dataset2[0:train_size, :], dataset2[train_size:, :]

    train_X, train_X_2, train_y = dataset_setup(dataset1=train_set, dataset2=dataset2, look_back=look_back, n=n,
                                                sentiment=sentiment)
    test_X, test_X_2, test_y = dataset_setup(dataset1=test_set, dataset2=dataset2, look_back=look_back, n=n,
                                             sentiment=sentiment)
    if sentiment:
        train_X = dataset_reshape(train_X)
        train_X_2 = dataset_reshape(train_X_2)
        train_X = np.concatenate((train_X, train_X_2), axis=2)
        test_X = dataset_reshape(test_X)
        test_X_2 = dataset_reshape(test_X_2)
        test_X = np.concatenate((test_X, test_X_2), axis=2)
    else:
        train_X = dataset_reshape(train_X)
        test_X = dataset_reshape(test_X)

    return train_X, train_y, test_X, test_y, train_set, test_set, train_set2, test_set2


def train_model(train_X, train_Y, states=50, epochs=None, batch_size=None, verbose=0,
                dropout=None, shuffle=False):
    """
    ML model: LSTM
    :return:
    """

    model = Sequential()
    # model.add(LSTM(states, batch_input_shape=(32, train_X.shape[1], train_X.shape[2]), return_sequences=True,
    #                stateful=False))
    model.add(LSTM(states, input_shape=(train_X.shape[1], train_X.shape[2]), return_sequences=True))
    model.add(Dropout(dropout))  # drop out regulation to first layer
    # model.add(LSTM(states, return_sequences=True, stateful=True))
    model.add(LSTM(states, stateful=False))
    model.add(Dropout(dropout))  # drop out regulation to second layer
    model.add(Dense(1))  # output file size
    model.add(Activation('tanh'))  # active function
    model.compile(loss='mse', optimizer='adam')

    # for i in range(epochs):
    model.fit(train_X, train_Y, batch_size=batch_size, epochs=epochs, verbose=verbose, shuffle=shuffle)
    # model.reset_states()

    return model


def accuracy_cal(true_data, prediction, steps_num=None):
    assert len(true_data) == len(prediction) * len(prediction[0])

    real = []
    pred = []
    for n, result in enumerate(prediction):
        l = len(result)
        indx = int(l / 2)
        pred.append(np.sign(result[0] - result[indx]))
        pred.append(np.sign(result[indx] - result[-1]))
        real_data = true_data[n * l: (n + 1) * l]
        real.append(np.sign(real_data[0] - real_data[indx]))
        real.append(np.sign(real_data[indx] - real_data[-1]))
    return np.sum(np.array(pred) == np.array(real)) / len(prediction) / 2


if __name__ == "__main__":
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # get rid of Tensorflow warnings
    # set random seeds to fix the results
    np.random.seed(123)
    # load price data
    df = pd.read_json('final_data.json', lines=True)

    df1 = df[-10300:-300]

    # only take the closing price and reverse the order (the newest one in the bottom)
    df1 = df1[['close']]  # .iloc[::-1]
    dataset1 = df1.values.astype('float32')

    # scale the data to be in range of -1 to 1 and thus could be fit in tanh activation function
    mms = MinMaxScaler(feature_range=(-1, 1))
    dataset1 = mms.fit_transform(dataset1)
    # print(len(dataset1))
    df2 = df[-10600:-600]
    # read sentiment analysis data
    # df = pd.read_csv('twitter_sentiment.csv', header=None)
    df2 = df2[['mean']]  # the mean tweets polarities per minute
    dataset2 = df2.values.astype('float32')
    # print(len(dataset2))

    # ---------------------------------------------------------------------------------------#
    #           multiple steps prediction with independent value prediction                 #
    # ---------------------------------------------------------------------------------------#
    # this method predicts each step individually with a separated model
    # advantage: error accumulation could be avoided
    # disadvantage: longer training time due to multiple models required

    # set time record
    time0 = time.time()
    # define several parameters for prediction
    steps_to_predict = 10  # how many steps to forecast
    num_pred = 10  # how many sequences to forecast
    split = 0.99  # splitting between train and test data
    look_back = 5  # how many data point to look back for prediction
    train_score = []
    test_score = []
    predictions = np.zeros((num_pred, steps_to_predict))
    sentiment = True
    # extract data for prediction and corresponding true label
    train_X, _, test_X, test_y, train_set, test_set, train_set2, test_set2 = dataset_gen(dataset1=dataset1,
                                                                                         dataset2=dataset2,
                                                                                         look_back=look_back,
                                                                                         train_test_split=split,
                                                                                         n=0,
                                                                                         sentiment=sentiment)

    # data used to predict comes from the last window of features
    # + further one in test dataset (size depends on lookback)
    pred_data = train_X[-1].reshape(1, look_back, 2 if sentiment else 1)  # change here

    for i in range(1, num_pred):
        if sentiment:
            pred_data1 = test_set[(i - 1) * look_back:i * look_back].reshape(1, look_back, 1)
            pred_data2 = test_set2[(i - 1) * look_back:i * look_back].reshape(1, look_back, 1)
            pred_data3 = np.concatenate((pred_data1, pred_data2), axis=2)
            pred_data = np.concatenate((pred_data, pred_data3), axis=0)
        else:
            pred_data3 = test_set[(i - 1) * look_back:i * look_back].reshape(1, look_back, 1)
            pred_data = np.concatenate((pred_data, pred_data3), axis=0)
        # change here
    # true data contains the last train data point + number of steps into the prediction data
    true_data = test_set[:steps_to_predict * num_pred]
    # print(len(true_data))
    # print(mms.inverse_transform(true_data.reshape(-1, 1)))
    # print(true_data)
    for j, data in enumerate(pred_data):
        for i in range(steps_to_predict):
            # generate proper 3D input for LSTM model training
            train_X, train_y, test_X, test_y, *_ = dataset_gen(dataset1=dataset1,
                                                               dataset2=dataset2,
                                                               look_back=look_back,
                                                               train_test_split=split,
                                                               n=i,
                                                               sentiment=sentiment)
            # modify the training data set
            # print(train_X.shape, train_y.shape)
            train_X = np.concatenate((train_X[j:], test_X[:j]), axis=0)
            train_y = np.concatenate((train_y[j:], test_y[:j]), axis=0)
            # print(train_X.shape, train_y.shape)
            # train model with generated dataset
            model = train_model(train_X, train_y, epochs=1, batch_size=128, dropout=0.2)
            # evaluate the model
            train_score.append(model.evaluate(train_X, train_y))
            test_score.append(model.evaluate(test_X, test_y))
            predictions[j, i] = model.predict(data.reshape(1, look_back, 2 if sentiment else 1))[0][0]  # change here

        # for j, data in enumerate(pred_data):
        #     print("pred_data:", data)
        #     predictions[j, i] = model.predict(data.reshape(1, look_back, 1))[0][0]

    # inverse transfer the data
    true_data = mms.inverse_transform(true_data.reshape(-1, 1))
    true_data = [x[0] for x in true_data.tolist()]
    # predictions = mms.inverse_transform(predictions)

    accuracy = accuracy_cal(true_data, predictions, steps_to_predict)

    # print results
    print("Average Training MSE:", np.average(train_score))
    print("Average Test MSE:", np.average(test_score))
    print("Accuracy:", accuracy)
    print("Runtime", round(time.time() - time0, 2), "secs")
    prediction_results = []
    for i, prediction in enumerate(predictions):
        prediction = mms.inverse_transform(prediction.reshape(-1, 1))
        prediction_results.append([x[0] for x in prediction])
    mse = np.sqrt(np.mean(np.square(np.array(prediction_results).flatten() - np.array(true_data))))
    print(mse)

    plt.plot(true_data, label='Actual Data')
    for i, prediction in enumerate(predictions):
        prediction = mms.inverse_transform(prediction.reshape(-1, 1))
        prediction = [x[0] for x in prediction]
        # print(prediction)
        padding = [None for p in range(i * steps_to_predict)]
        # print(padding)
        plt.plot(padding + prediction, label='Prediction')
    plt.legend(loc='best')
    plt.xlabel('Time (minute)')
    plt.ylabel('Price (USD)')
    plt.title('Independent Value Multiple Steps Forecasting')
    plt.savefig('MSF_{:}.png'.format(str(sentiment)))
    # plt.show()

# ==============================================================================================================#

# train_X, train_y, test_X, test_y = dataset_gen(dataset1=dataset1, dataset2=dataset2)
# score, pred_train, pred_test = pred_model(train_X, train_y, test_X, test_y)
# test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
# test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
# mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
# print('Test RMSE: %.3f' % mse_test)

# test drop-out
# drop_out = np.arange(0.0, 0.55, 0.05)
# dropout_list = list()
# for i in drop_out:
#     score, pred_train, pred_test, history_callback = pred_model(train_X, train_y, test_X, test_y, dropout=i)
#     test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
#     test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
#     mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
#     print('Test RMSE: %.3f' % mse_test)
#     dropout_list.append(mse_test)
# print(dropout_list)
# plt.figure(figsize=(8, 8))
# plt.plot(drop_out, dropout_list)
# plt.xlabel('Percent of dropout', fontsize=20)
# plt.ylabel('MSE', fontsize=20)
# plt.title('MSE vs dropout', fontsize=24)
# plt.savefig("drop_out.png", dpi=300)

# test look_back
# look_back_list = list()
# for i in range(1, 2):
#     train_X, train_y, test_X, test_y, train_set, test_set = dataset_gen(dataset1=dataset1, dataset2=dataset2, look_back=1)
#     score, model, history_callback = train_model(train_X, train_y, test_X, test_y)
#
#     test_pred_inv = mms.inverse_transform(model.predict(test_X, batch_size=1).reshape(1, -1))
#     test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
#     mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
#     print('Test RMSE: %.3f' % mse_test)
#     look_back_list.append(mse_test)
# print(look_back_list)
# plt.figure(figsize=(8, 8))
# plt.plot([i for i in range(1, 2)], look_back_list)
# plt.xlabel('No. of look_back', fontsize=20)
# plt.ylabel('MSE', fontsize=20)
# plt.title('MSE vs look_back', fontsize=24)
# plt.savefig("look_back.png", dpi=300)

# no of states test
# states_para = dict()
# states_list = list()
# for i in range(1, 111, 5):
#     t0 = time()
#     score, pred_train, pred_test, history_callback = pred_model(train_X, train_y, test_X, test_y, states=i)
#     t = time() - t0
#     print(score)
#     test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
#     test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
#     mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
#     print('Test RMSE: %.3f' % mse_test)
#     states_list.append(mse_test)
#     states_para[i] = (mse_test, t)
# print(states_para)
# plt.figure(figsize=(8, 8))
# plt.plot([i for i in range(1, 111, 5)], states_list)
# plt.xlabel('No. of states', fontsize=20)
# plt.ylabel('MSE', fontsize=20)
# plt.title('MSE vs states', fontsize=24)
# plt.savefig("states.png", dpi=300)

# no of epoches test
# epoch = 100
# score, pred_train, pred_test, history_callback = pred_model(train_X, train_y, test_X, test_y, epochs=epoch)
# test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
# test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
# mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
# loss_history = history_callback.history["loss"]
# print(loss_history)
# print('Test RMSE: %.3f' % mse_test)
# plt.figure(figsize=(8, 8))
# plt.plot(list(range(epoch)), loss_history)
# plt.xlabel('No. of epochs', fontsize=20)
# plt.ylabel('loss', fontsize=20)
# plt.title('MSE vs epochs', fontsize=24)
# plt.savefig("epochs.png", dpi=300)

# no of batches test
# batch_para = dict()
# batch_list = list()
# batch = [2 ** i for i in range(4, 9)]
# for i in batch:
#     print(i)
#     t0 = time()
#     score, pred_train, pred_test,  history_callback = pred_model(train_X, train_y, test_X, test_y, batch_size=i)
#     t = time() - t0
#     # print(score)
#     test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
#     test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
#     mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
#     print('Test RMSE: %.3f' % mse_test)
#     batch_list.append(mse_test)
#     batch_para[i] = (mse_test, t)
# print(batch_para)
# plt.figure(figsize=(8, 8))
# plt.plot(batch, batch_list)
# plt.xlabel('No. of batches', fontsize=20)
# plt.ylabel('MSE', fontsize=20)
# plt.title('MSE vs batches', fontsize=24)
# plt.savefig("batches.png", dpi=300)

# train_pred = mms.inverse_transform(pred_train.reshape(1, -1))
# train_y = mms.inverse_transform(train_y.reshape(1, -1))
