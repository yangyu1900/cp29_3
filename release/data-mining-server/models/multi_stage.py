import pandas as pd
import numpy as np
from numpy import fliplr
from numpy import flipud
from numpy import array
from pandas.io.json import json_normalize
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import json
import math
import pymongo
from pymongo import MongoClient
import time
from sklearn import metrics
from sklearn.metrics import confusion_matrix

def connect_database():
 
    client = MongoClient('localhost', 27017)
    db = client['bitcoin']
    collection = db['final_data2']
    return db, collection

def data_extraction(filed, collection):
    '''
    extract a certain filed('close') of data from the collecttion 
    '''

    data = []
    for doc in list(collection.find())[-10300:-300]:
        data.append(doc[filed])
    data = array(data)
    #data = flipud(data)
    return data

    

def feature_engineering(df):
    """
    preprocess data for better result
    :return:
    """
def take_by_window(dataset, window):
    x, y = [], []
    for i in range(len(dataset) - window ):        
        x.append(list(dataset[i:(i + window)]))
        y.append(dataset[i + window,])
    x = np.array(x)
    print(x.shape)
    y = np.array(y)
    print(x.shape[0],x.shape[1])
    x = np.reshape(x, (x.shape[0] ,1 , x.shape[1]))
    print(x.shape)
    return x, y

def pred_model(test, x_train, y_train, step_size=1, num_features=3, epochs=100, batch_size=128, verbose=2, pred_num=1000):
    """
    ML model: LSTM
    Look_back: how many point
    :return:
    """

    model = Sequential()
    model.add(LSTM(250, input_shape=(step_size, num_features), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(200))
    model.add(Dense(1))  # output file size
    model.add(Activation('tanh'))  # active function
#     model.add(Dropout(0.05))  # drop out regulation
    model.compile(loss='mse', optimizer='adam')
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, verbose=verbose)
    
    

    y_test = []

    
    for i in range(pred_num):
        if i%10 == 0:
            pred_feature = test[i]
            pred_feature = np.reshape(pred_feature, (1 ,1 , num_features))
            pred_data = model.predict(pred_feature)
            y_test.append(pred_data)
            pred_feature = np.append(pred_feature[0,0,1:], pred_data)
            pred_feature = np.reshape(pred_feature, (1 ,1 , num_features))
        else:
            pred_data = model.predict(pred_feature)
            y_test.append(pred_data)
            pred_feature = np.append(pred_feature[0,0,1:], pred_data)
            pred_feature = np.reshape(pred_feature, (1 ,1 , num_features))

    pred_train = model.predict(x_train)
    #pred_test = model.predict(x_test)
    #pred_dataset = array(pred_dataset)
    #print(array(x_test).shape, array(y_test).shape)
    
    #score = model.evaluate(array(x_test[0:-1]), y_test, batch_size=128)

    return pred_train , np.reshape(array(y_test), (pred_num))
def accuracy_cal(y_test, pred_test):
    assert len(y_test) == len(pred_test) * len(pred_test[0])
    pred = []
    true = []
    for i in range(0, len(pred_test), 10):
        if pred_test[i+5] >= pred_test[i]:
            pred.append(1)
        else:
            pred.append(0)
        if pred_test[i+9] >= pred_test[i+5]:
            pred.append(1)
        else:
            pred.append(0)
            
        if y_test[i+5] >= y_test[i]:
            true.append(1)
        else:
            true.append(0)
        if y_test[i+9] >= y_test[i+5]:
            true.append(1)
        else:
            true.append(0)
    
    return true, pred

if __name__ == "__main__":
    '''connect to DB: bitcoin and collection: btc'''
    np.random.seed(123)
    db, collection = connect_database()
    df = data_extraction('close', collection)
    t = data_extraction('_id', collection)
    #stm = data_extraction('mean', collection)
    dataset = df.astype('float32')
    #time = time.astype('int32')
    mms = MinMaxScaler(feature_range=(0, 1))

    '''construct a scale based on dataset'''
    dataset = mms.fit_transform(dataset)

    '''define the size of lookback'''
    window = 600
    train_size = int(len(dataset)*0.99)
    test_size = len(dataset)-train_size
    '''prepare the training data'''
    x, y = take_by_window(dataset, window)
    x_train, y_train = x[:train_size-window], y[:train_size-window]
    x_test, y_test = x[train_size-window:], y[train_size-window:]
    '''get the predict data'''
    start = time.time()
    pred_train, pred_test = pred_model(x_test, x_train, y_train, step_size=1, num_features=window, epochs=15, batch_size=128, verbose=2, pred_num = test_size)
    #print(score)
    run = time.time()-start
    '''inverse the scale'''
    pred_train = mms.inverse_transform(pred_train)
    y_train = mms.inverse_transform(y_train)
    pred_test = mms.inverse_transform(pred_test)
    y_test = mms.inverse_transform(y_test)
    
    plt.figure(figsize=(10,8))
    for i in range(10):
        label = "test"+str(i)
        plt.plot(range(i*10,(i+1)*10),pred_test[i*10:(i+1)*10],label = label)
    plt.plot(range(100), df[-test_size:], label = 'Actual')
    plt.xlabel('Time (minute)')
    plt.ylabel('Price (USD)')
    plt.legend()
    plt.show()
    
   
    con_m = confusion_matrix(y_test, pred_test)
    print(con_m)
    print("Prediction score:\n%s" % metrics.classification_report(real, pred))
    
    trainScore = math.sqrt(mean_squared_error(y_train, pred_train[:, 0]))
    print('Train Score: %.2f MSE' % (trainScore))
    testScore = math.sqrt(mean_squared_error(y_test, pred_test[:]))
    print('Test Score: %.2f MSE' % (testScore))
    print('Run Time: ', run_time)

