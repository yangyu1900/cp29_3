import pandas as pd
import numpy as np
from numpy import fliplr
from numpy import flipud
from numpy import array
from pandas.io.json import json_normalize
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import json
import math
import pymongo
from pymongo import MongoClient
import time
from sklearn import metrics
from sklearn.metrics import confusion_matrix

def connect_database():
 
    client = MongoClient('localhost', 27017)
    db = client['bitcoin']
    collection = db['final_data2']
    return db, collection

def data_extraction(filed, collection):
    '''
    extract a certain filed('close') of data from the collecttion 
    '''

    data = []
    for doc in list(collection.find())[-10300:]:
        data.append(doc[filed])
    data = array(data)
    #data = flipud(data)
    return data

    
def take_by_window(dataset, window):
    x, y = [], []
    for i in range(len(dataset) - window ):
        
        x.append(dataset[i:(i + window)])
        y.append(dataset[i + window,])
    x = np.array(x)
    print(x.shape)
    y = np.array(y)
    print(x.shape[0],x.shape[1])
    x = np.reshape(x, (x.shape[0] ,1 , x.shape[1]))
    print(x.shape)
    return x, y

def pred_model(x_train, y_train, step_size=1, num_features=3, epochs=100, batch_size=128, verbose=2, pred_num=1000):
    """
    ML model: LSTM
    Look_back: how many point
    :return:
    """

    model = Sequential()
    model.add(LSTM(250, input_shape=(step_size, num_features), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(200))
    model.add(Dense(1))  # output file size
    model.add(Activation('tanh'))  # active function
#     model.add(Dropout(0.05))  # drop out regulation
    model.compile(loss='mse', optimizer='adam')
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, verbose=verbose)
    
    
    
    pred_feature = x_train[-1]
    print(pred_feature.shape)
    print(type(pred_feature))
    pred_feature = np.reshape(pred_feature, (1 ,1 , num_features))
    print(pred_feature.shape)
    x_test = list([x_train[-1]])
    #pred_num = 1000
    y_test = []
    #x_test = []
    
    for i in range(pred_num):
        pred_data = model.predict(pred_feature)
        print(pred_data)
        y_test.append(pred_data)
        pred_feature = np.append(pred_feature[0,0,1:], pred_data)
        #print(pred_feature)
        pred_feature = np.reshape(pred_feature, (1 ,1 , num_features))
        #x_test.append(pred_feature)
    pred_train = model.predict(x_train)
    #pred_test = model.predict(x_test)
    #pred_dataset = array(pred_dataset)
    #print(array(x_test).shape, array(y_test).shape)
    
    #score = model.evaluate(array(x_test[0:-1]), y_test, batch_size=128)

    return pred_train , np.reshape(array(y_test), (pred_num))


def accuracy_cal(y_test, pred_test):
    assert y_test.size == pred_test.size * pred_test[0].size
    pred = []
    true = []
    for i in range(0, len(pred_test), 10):
        if pred_test[i+5] >= pred_test[i]:
            pred.append(1)
        else:
            pred.append(0)
        if pred_test[i+9] >= pred_test[i+5]:
            pred.append(1)
        else:
            pred.append(0)
            
        if y_test[i+5] >= y_test[i]:
            true.append(1)
        else:
            true.append(0)
        if y_test[i+9] >= y_test[i+5]:
            true.append(1)
        else:
            true.append(0)
    
    return true, pred

if __name__ == "__main__":
    np.random.seed(1234)
    db, collection = connect_database()
    df = data_extraction('close', collection)
    t = data_extraction('_id', collection)
    stm = data_extraction('mean', collection)
    dataset = df.astype('float32')
    t = t.astype('int32')
    mms = MinMaxScaler(feature_range=(0, 1))

    '''construct a scale based on dataset'''
    dataset = mms.fit_transform(dataset)

    '''define the size of lookback'''
    window = 600
    
    train_size = int(len(dataset))-400
    test_size = 400#len(dataset)-train_size
    train_set, test_set = dataset[0:train_size], dataset[-400:]
    x_train, y_train = take_by_window(train_set, window)
    y_test = test_set

    '''make prediction'''
    start = time.time()
    pred_train, pred_test = pred_model(x_train, y_train, step_size=1, num_features=window, epochs=15, batch_size=128, verbose=2, pred_num = test_size)
    #print(score)
    run_time = time.time()-start

    '''inverse the scale'''
    pred_train = mms.inverse_transform(pred_train)
    y_train = mms.inverse_transform(y_train)
    pred_test = mms.inverse_transform(pred_test)
    y_test = mms.inverse_transform(y_test)


    real, pred = accuracy_cal(y_test, pred_test)    
    con_m = confusion_matrix(real, pred)
    print(con_m)
    print("Prediction score:\n%s" % metrics.classification_report(real, pred))

    plt.figure(figsize=(10,8))
    plt.plot(range(400), y_test.tolist(), label='Actual')
    plt.plot(range(400), pred_test.tolist(), label='Testing')
    plt.xlabel('Time (minute)')
    plt.ylabel('Price (USD)')
    plt.legend(loc='best')
    plt.show()

    plt.plot(range(100), y_test[0:100].tolist(),label = 'actual')    
    plt.plot(range(100), pred_test[0:100].tolist(),label="predict")
    plt.figure(figsize=(10,8))
    plt.xlabel('Time (minute)')
    plt.ylabel('Price (USD)')
    plt.legend(loc='best')
    plt.show()

    v = list(map(lambda x: abs(float(x[0])-x[1]), zip(df[-400:-300].astype('float32'), pred_test))) 
    plt.figure(figsize=(10,8))
    plt.plot(range(100),v,label = 'error')
    plt.xlabel('Time (minute)')
    plt.ylabel('Error(USD)')
    plt.legend()
    plt.show()
    
    trainScore = math.sqrt(mean_squared_error(y_train[:100], pred_train[:100, 0]))
    print('Train Score: %.2f MSE' % (trainScore))
    testScore = math.sqrt(mean_squared_error(y_test[:100], pred_test[:100]))
    print('Test Score: %.2f MSE' % (testScore))
    print('Run Time: ', run_time)
    
    
    
