#!/usr/bin/python

import pandas as pd
from sklearn.preprocessing import MinMaxScaler

df = pd.read_json('historic_rates.json', lines=True)
# print(df.shape)
# df = df[['id','close','high','low','open','time','volume']]
df = df[:10000]# take first 10000 lines of data
df= df[['close']] # only look at close price
dataset = df.values.astype('float32')
# print(dataset)
# transfer the dataset to be ranged from 0-1 thus could be used in the active function
dataset = MinMaxScaler(feature_range=(0, 1)).fit_transform(dataset)
print(dataset)

