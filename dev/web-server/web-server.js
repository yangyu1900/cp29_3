var express = require("express");
var router = require("./app/routers/router.js");

var app = express();

app.use("/", router);

app.listen(8000, function() {
	console.log("web-server is listening on port 8000");
});
