import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable()
export class UserService {

  private isUserLoggedIn;
  public username;
  private password;
  constructor() {
    this.isUserLoggedIn = false;
  }

 loggedIn(userInfo: User) {
    this.isUserLoggedIn = true;
    this.username = userInfo.username;
    this.password = userInfo.password;
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }
}
