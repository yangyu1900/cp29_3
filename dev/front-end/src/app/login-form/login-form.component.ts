import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  userInfo: User = new User("","");
  constructor( private router:Router, private user: UserService) { }

  ngOnInit() {
  }

  loginUser() {
    console.log(this.userInfo);
    if(this.userInfo.username==='admin'&&this.userInfo.password==='admin'){
      this.user.loggedIn(this.userInfo);
      this.router.navigate(['dashboard']);
    }
  }

}
