document.addEventListener('DOMContentLoaded', function () {
  particleground(document.getElementById('background'), {
    dotColor: '#dbdbdb',
    lineColor: '#dbdbdb',
    density: 9000,
    particleRadius: 6,
    lineWidth: 1.2,
  });
  var intro = document.getElementById('login');
  intro.style.marginTop = - intro.offsetHeight / 2 + 'px';
}, false);
