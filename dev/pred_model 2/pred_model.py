#!/usr/bin/python

import pandas as pd
from pandas.io.json import json_normalize
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation
import json

def data_extraction(filename):
    """
    Use for data-extraction (JSON to pandas df?)
    Including the train/test data splitting
    :return:
    """
    df = pd.read_json(filename, lines=True)
    print(df.shape)

def feature_engineering(df):
    """
    preprocess data for better result
    :return:
    """


def pred_model(x_train, y_train, x_test, y_test, step_size, num_features, epochs=200, batch_size=128, verbose=2):
    """
    ML model: LSTM
    Look_back: how many point
    :return:
    """

    model = Sequential()
    model.add(LSTM(4, batch_input_shape=(1, step_size, num_features)))
    model.add(Dense(1))  # output file size
    model.add(Activation('tanh'))  # active function
    model.add(Dropout(0.1))  # drop out regulation
    model.compile(loss='mse', optimizer='adam')
    for epoch in range(epochs):
        model.fit(x_train, y_train, epochs=1, batch_size=1, verbose=verbose)
    score = model.evaluate(x_test, y_test, batch_size=128)

    return score


if __name__ == "__mian__":
    df = pd.read_json('historic_rates.json', lines=True)
    print(df.shape)